source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
HISTFILE=~/.zsh_history
HISTSIZE=100000
SAVEHIST=100000

PS1="%F{403434}%------------------------------------------------------------
%F{087DC0}%~ %F{847BE4}» "

neofetch --size 362 --w3m ~/Downloads/neofetch/arch-$(cat ~/.fehbg | sed -e "s|#\!/bin/sh||g" | sed -e "s|'||g" | sed -e "s|feh --bg-fill||g" | sed -e "s|$HOME/Downloads/Wall/||g" | sed -e "/^$/d" | sed -e "s| ||g")

export EDITOR='vim'

transfer() { 
    if [ $# -eq 0 ]; 
    then 
        echo "No arguments specified. Usage:\necho transfer /tmp/test.md\ncat /tmp/test.md | transfer test.md"
        return 1
    fi
    tmpfile=$( mktemp -t transferXXX )
    file=$1
    if tty -s; 
    then 
        basefile=$(basename "$file" | sed -e 's/[^a-zA-Z0-9._-]/-/g') 
        if [ ! -e $file ];
        then
            echo "File $file doesn't exists."
            return 1
        fi
        if [ -d $file ];
        then
            zipfile=$( mktemp -t transferXXX.zip )
            cd $(dirname $file) && zip -r -q - $(basename $file) >> $zipfile
            curl --progress-bar --upload-file "$zipfile" "https://transfer.sh/$basefile.zip" >> $tmpfile
            rm -f $zipfile
        else
            curl --progress-bar --upload-file "$file" "https://transfer.sh/$basefile" >> $tmpfile
        fi
    else 
        curl --progress-bar --upload-file "-" "https://transfer.sh/$file" >> $tmpfile
    fi
    cat $tmpfile
    rm -f $tmpfile
}

extract () {
if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xvjf $1    ;;
      *.tar.gz)    tar xvzf $1    ;;
      *.tar.xz)    tar xvJf $1    ;;
      *.bz2)       bunzip2 $1     ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1      ;;
      *.tar)       tar xvf $1     ;;
      *.tbz2)      tar xvjf $1    ;;
      *.tgz)       tar xvzf $1    ;;
      *.zip)       unzip $1       ;;
      *.Z)         uncompress $1  ;;
      *.7z)        7z x $1        ;;
      *.xz)        unxz $1        ;;
      *.exe)       cabextract $1  ;;
      *)           echo "\`$1': unrecognized file compression" ;;
    esac
  else
    echo "\`$1' is not a valid file"
  fi
}

alias ...='cd /'
alias update='pacaur -Syu --noconfirm; sudo pacman -Rns $(sudo pacman -Qttdq) --noconfirm; pacaur -Sc --noconfirm'
alias moreupdate='pacaur -Syu --noconfirm; sudo pacman -Rns $(sudo pacman -Qttdq) --noconfirm; pacaur -Scc; rm -rf ~/.cache/*'
alias pacman='sudo pacman'
alias pacaur='pakku'
unsetopt beep
